﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevisaoAula
{
    class Conta
    {

        private int numero;

        private DateTime dataVencimento, dataPagamento;

        private double valor, valorPagamento;


        public void pagar (double valorPago)
        {
            if (valorPago < this.valor)
            {
                throw new Exception("O Valor recebido foi menor que o valor da conta");
            }
            else
            {
                this.valorPagamento = valorPago ;
                this.dataPagamento = DateTime.Now;
            }
        }
        public int Numero 
        {
            get { return numero; }
            set { numero = value; }
        }

        public DateTime Vencimento
        {
            get { return dataVencimento; }
            set { dataVencimento = value; }
        }

        public double Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        public DateTime Pagamento
        {
            get {return dataPagamento; }
            set { dataPagamento = value; }
            
        }

        public double ValorPago
        {
            get { return valorPagamento; }
            set { valorPagamento = value; }
        }
        
        
    }
}
