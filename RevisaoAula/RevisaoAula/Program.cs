﻿using System;
using System.Collections.Generic;

namespace RevisaoAula
{
    class Program
    {
        static void Main(string[] args)
        {
            //var dt1 = new DateTime(2022, 03, 25);
            //var dt2 = new DateTime(2022, 03, 30);
            //var dt3 = new DateTime(2022, 03, 25);

            //Conta c1 = new Conta
            //{
            //    Numero = 123,
            //    Vencimento = dt1,
            //    Valor = 125.50
            //};

            //Conta c2 = new Conta();
            //c2.Numero = 456;
            //c2.Vencimento = dt2;
            //c2.Valor = 160.78;

            //Conta c3 = new Conta
            //{
            //    Numero = 789,
            //    Vencimento = dt3,
            //    Valor = 200.00
            //};



            //Console.Write("Informe o valor que será pago na primeira conta: ");
            //c1.pagar(Convert.ToDouble(Console.ReadLine()));

            //Console.Write("Informe o valor que será pago na segunda conta: ");
            //c2.pagar(Convert.ToDouble(Console.ReadLine()));

            //try
            //{
            //    Console.Write("Informe o valor que será pago na terceira conta: ");
            //    c3.pagar(Convert.ToDouble(Console.ReadLine()));
            //}
            //catch (Exception ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            //Console.ReadKey();

            //var conta = RetornaConta();

            do
            {
                Menu();
                Console.WriteLine("Deseja sair? (S/N): ");
                string resposta = Console.ReadLine().ToUpper();
            } while (!Console.ReadLine().ToUpper().StartsWith("S"));
        }

        private static void Menu()
        {
            int opcao;

            do
            {
                Console.WriteLine("O que você deseja fazer?");
                Console.WriteLine("1- Cadastrar Conta");
                Console.WriteLine("2- Pagar Conta");
                Console.WriteLine("3- Listar Conta");
                opcao = Convert.ToInt32(Console.ReadLine());

                if (opcao == 1)
                    contas.Add(RetornaConta());

                else if (opcao == 2)
                {
                    pagarConta();
                }
                else if (opcao == 3)
                {
                    listarContas();
                }

            } while (OpcaoInvalida(opcao));
                
        }

        private static void listarContas()
        {
            foreach (Conta conta in contas)
            {
                Console.WriteLine("\nNúmero: " + conta.Numero);
                Console.WriteLine("Data de Vencimento: " + conta.Vencimento);
                Console.WriteLine("Valor: " + conta.Valor);
                Console.WriteLine("Data de Pagamento:  " + conta.Pagamento);
                Console.WriteLine("Valor Pago: " + conta.ValorPago + "\n");

            }
        }

        private static void pagarConta()
        {
            try
            {
                listarContas();
                Console.WriteLine("Digite o número da conta que deseja pagar: ");
                Conta contaSelecionada = buscarConta(Convert.ToInt32(Console.ReadLine()));

                if(contaSelecionada == null)
                {
                    Console.WriteLine("=======================");
                    Console.WriteLine("Conta não encontrada.");
                    Console.WriteLine("=======================");
                }
                else
                {
                    Console.WriteLine("=======================");
                    Console.WriteLine("Conta encontrada.");
                    Console.WriteLine("=======================");

                    Console.WriteLine("Qual o valor que deseja pagar?");
                    double valor = Convert.ToDouble(Console.ReadLine());

                    contaSelecionada.pagar(valor);

                }
                
            }
            catch(Exception ex)
            {
                Console.WriteLine("===================");
                Console.WriteLine("ERRO!");
                Console.WriteLine(ex.Message);
                Console.WriteLine("===================");
            }
            
        }

        private static Conta buscarConta(int numeroConta)
        {
            foreach(Conta conta in contas)
            {
                if(conta.Numero == numeroConta)
                {
                    return conta;
                }
            }

            return null;
        }

        private static bool OpcaoInvalida(int opcao)
        {
            return opcao != 1 && opcao != 2;
        }

        private static Conta RetornaConta()
        {
            Conta conta = new Conta();

            Console.Write("Informe o número da conta: ");
            conta.Numero =Convert.ToInt32(Console.ReadLine());

            Console.Write("Informe o valor da conta: ");
            conta.Valor = Convert.ToDouble(Console.ReadLine());

            Console.Write("Informe a data de vencimento da conta: ");
            conta.Vencimento = Convert.ToDateTime(Console.ReadLine());


            return conta;
        } 

        private static List<Conta> contas = new List<Conta>();

    }
}
